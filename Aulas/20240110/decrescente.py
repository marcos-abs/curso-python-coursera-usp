#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: decrescente.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Wednesday, 10 January 2024 11:55:22
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Wednesday, 10 January 2024 11:59:24
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············
# * *****
###

decrescente = True
anterior = int(input("Digite o primeiro número da sequência: "))
valor = 1
while valor != 0 and decrescente:
  valor = int(input("Digite o próximo número da sequência: "))
  if valor > anterior:
    decrescente = False
  anterior = valor
if decrescente:
  print("A sequência está em ordem decrescente! :-) ")
else:
  print("A sequência não está em ordem decrescente! :-( ")