#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: exercícios2.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Wednesday, 10 January 2024 12:33:04
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Tuesday, 16 January 2024 10:42:04
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Receba um número inteiro positivo na entrada e imprima os "n" primeiros
# * ············ números ímpares naturais.
# * *****
###

def impares(n):
  i = 0
  resultado = []
  while i < n:
    x = 2 * i + 1
    if x is None:
      break
    resultado.append(x)
    i = i + 1
  return resultado

def imprimir(lista):
  for i in lista:
    print(i)

valor = int(input("Digite o valor de n: "))
imprimir(impares(valor))
