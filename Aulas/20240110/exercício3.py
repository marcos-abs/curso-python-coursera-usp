#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: exercício3.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Wednesday, 10 January 2024 12:44:19
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Wednesday, 10 January 2024 12:49:14
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Escreva um programa que receba um número inteiro na entrada, calcule e
# * ············ imprima a soma dos dígitos deste número na saída
# * *****
###

def calcular(n):
  soma = 0
  while n > 0:
    resto = n % 10
    n = n // 10
    soma = soma + resto
  return soma

valor = int(input("Digite um número inteiro: "))
print(calcular(valor))