#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: exercício1.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Wednesday, 10 January 2024 12:27:59
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Wednesday, 10 January 2024 12:31:51
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Escreva um programa que receba um número natural "n" na entrada e imprima "n!" (fatorial) na saída
# * *****
###

def fatorial(n):
  fat = 1
  while n > 1:
    fat = fat * n
    n = n - 1
  return fat

valor = int(input("Digite o valor de n: "))
print(fatorial(valor))
