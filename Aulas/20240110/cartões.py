#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: cartões.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Wednesday, 10 January 2024 12:05:07
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Wednesday, 10 January 2024 12:06:37
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············
# * *****
###

meuCartão = int(input("Digite o número do ser cartão de crédito: "))
cartãoLido = 1
encontreiMeuCartãoNaLista = False
while cartãoLido != 0 and not encontreiMeuCartãoNaLista:
  cartãoLido = int(input("Digite o número do próximo cartão de crédito: "))
  if cartãoLido == meuCartão:
    encontreiMeuCartãoNaLista = True
if encontreiMeuCartãoNaLista:
  print("Eba! Meu cartão está lá!")
else:
  print("Xi, meu cartão não está lá...")