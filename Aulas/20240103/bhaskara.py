#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: bhaskara.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Wednesday, 03 January 2024 10:18:04
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Wednesday, 03 January 2024 10:33:11
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············
# * *****
###

import math

a = float(input("Digite o valor de a:"))
b = float(input("Digite o valor de b:"))
c = float(input("Digite o valor de c:"))

delta = b ** 2 - 4 * a * c

if delta ==0:
    raiz1 = (-b + math.sqrt(delta)) / (2 * a)
    print("A única raiz possível é: ", raiz1)
else:
    if delta < 0:
        print("Esta equação não possui raízes reais")
    else:
        raiz1 = (-b + math.sqrt(delta)) / (2 * a)
        raiz2 = (-b - math.sqrt(delta)) / (2 * a)
        print("A primeira raiz é: ", raiz1, " e a segunda é: ", raiz2)
