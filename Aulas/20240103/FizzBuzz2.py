#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 11:09:47 2024

@author: marcos
"""


valor = int(input("Digite um número inteiro: "))
if valor % 3 == 0 and valor % 5 == 0:
    print("FizzBuzz")
else:
    print(valor)
