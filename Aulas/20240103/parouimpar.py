#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 10:56:52 2024

@author: marcos
"""

valor = int(input("Digite um valor para que eu diga se é par ou ímpar: "))
if valor % 2 == 0:
    print("\nO valor", valor, "é par")
else:
    print("\nO valor", valor, "é ímpar")
