#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 11:12:27 2024

@author: marcos
"""

valor1 = int(input("Digite o primeiro número inteiro: "))
valor2 = int(input("Digite o segundo número inteiro: "))
valor3 = int(input("Digite o terceiro número inteiro: "))
if valor1 < valor2 and valor2 < valor3:
    print("crescente")
else:
    print("não está em ordem crescente")
    