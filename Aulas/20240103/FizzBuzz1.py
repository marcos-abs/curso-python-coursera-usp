#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 11:07:27 2024

@author: marcos
"""


valor = int(input("Digite um número inteiro: "))
if valor % 5 == 0:
    print("Buzz")
else:
    print(valor)
