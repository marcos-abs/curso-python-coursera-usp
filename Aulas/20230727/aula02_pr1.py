#!python3.7
###
# *****
# File: aula02_pr1.py
# Project: Curso de Python Coursera USP
# Path: ~desenvolvedor/curso-de-python-coursera-usp
# File Created: Wednesday, 27 July 2022 00:20:29
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Wednesday, 27 July 2022 00:20:32
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Exercício 2.2
# *****
###
def main():
    num = int(input("Digite um inteiro: "))
    soma = 0

    while num != 0:
        soma = soma + num
        num = int(input("Digite um inteiro: "))

    print("A soma eh", soma)


#----------------------------------------------
main()