#!python3.7
###
# *****
# File: somador.py
# Project: Curso de Python Coursera USP
# Path: ~desenvolvedor/curso-de-python-coursera-usp
# File Created: Monday, 25 July 2022 19:18:26
# Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
# -----
# Last Modified: Monday, 25 July 2022 19:52:37
# Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
# -----
# Copyright (c) 2022 All rights reserved, Marcant Tecnologia da Informação
# -----
# Description:
# ············ Meu primeiro programa em Python
# *****
###

a = 10
b = 20

soma = a + b

print("A soma de", a, "e", b, "é", soma)
