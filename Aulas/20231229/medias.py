#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: medias.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Friday, 29 December 2023 12:41:56
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Friday, 29 December 2023 12:48:37
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2023 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Faça um programa em Python que receba quatro notas, calcule e imprima
# * a média aritmética.
# * *****
###

def main():
  nota1 = int(input("Digite a primeira nota:"))
  nota2 = int(input("Digite a segunda nota:"))
  nota3 = int(input("Digite a terceira nota:"))
  nota4 = int(input("Digite a quarta nota:"))
  print("A média aritmética é", (nota1 + nota2 + nota3 + nota4) / 4)

main()
