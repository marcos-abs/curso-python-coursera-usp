#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: exercicio3.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Friday, 29 December 2023 14:55:28
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Friday, 29 December 2023 14:58:28
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2023 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Faça um programa em Python que recebe um número inteiro e imprime seu dígito das dezenas.
# * *****
###

def main():
  numero = int(input("Digite um número inteiro: "))
  dezena = (numero // 10) % 10
  print("O dígito das dezenas é", dezena)

main()