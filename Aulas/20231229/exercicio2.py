#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: exercicio2.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Friday, 29 December 2023 14:51:35
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Friday, 29 December 2023 14:53:58
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2023 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Reescreva o programa contaSegundos para imprimir também a quantidade de dias, ou seja,
# * faça um programa em Python que, dada a quantidade de segundos, "quebre" esse valor em dias, horas,
# * minutos e segundos. A saída deve estar no formato: a dias, b horas, c minutos e d segundos. Seja
# * cuidadoso com o formato! Espaços a mais, vírgulas faltando ou outras diferenças são considerados erro
# * *****
###
def main():
  segundos = int(input("Por favor, entre com o número de segundos que deseja converter: "))
  dias = segundos // 86400    # 1 dia = 24 horas * 60 minutos * 60 segundos
  segundos = segundos % 86400
  horas = segundos // 3600    # 1 hora = 60 minutos * 60 segundos
  segundos = segundos % 3600
  minutos = segundos // 60    # 1 minuto = 60 segundos
  segundos = segundos % 60
  print(dias, "dias,", horas, "horas,", minutos, "minutos e", segundos, "segundos.")

main()