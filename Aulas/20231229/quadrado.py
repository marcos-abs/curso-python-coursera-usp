#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: quadrado.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Friday, 29 December 2023 12:33:10
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Friday, 29 December 2023 12:48:16
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2023 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Faça um programa em Python que receba (entrada de dados) o valor correspondente
# * ao lado de um quadrado, calcule e imprima (saída de dados) seu perímetro e sua área.
# *
# * Observação: a saída deve estar no formato: "perímetro: x - área: y"
# * *****
###

def main():
    valor = input("Digite o valor correspondente ao lado de um quadrado: ")
    inteiro = int(valor)

    # calcule a área
    area = inteiro ** 2

    # calcule o perimetro
    perimetro = inteiro * 4

    # imprima o resultado
    print("perímetro:", perimetro, "- área:", area)

main()