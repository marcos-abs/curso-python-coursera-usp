#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: exercicio1.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Friday, 29 December 2023 12:56:24
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Friday, 29 December 2023 14:50:27
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2023 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Escreva um programa que receba (entrada de dados através do teclado) o
# * nome do cliente, o dia de vencimento, o mês de vencimento e o valor da fatura  e
# * imprima (saída de dados) a mensagem com os dados recebidos, no mesmo formato da
# * mensagem acima. Note que o programa imprime a saída em duas linhas diferentes.
# * Note também que, como não é preciso realizar cálculos, o valor não precisa ser
# * convertido para número, pode ser tratado como texto.
# * *****
###

def main():
  nome = input("Digite o nome do cliente: ")
  dia = input("Digite o dia de vencimento: ")
  mes = input("Digite o mês de vencimento: ")
  valor = input("Digite o valor da fatura: ")

  print("Olá,", nome)
  print("A sua fatura com vencimento em", dia, "de", mes, "no valor de R$", valor, "está fechada.")

main()
