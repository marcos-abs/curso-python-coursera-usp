#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: test_vogais.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Tuesday, 16 January 2024 20:37:03
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Tuesday, 16 January 2024 20:43:50
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Escreva a função vogal que recebe um único caractere como parâmetro e devolve
# * True se ele for uma vogal e False se for uma consoante.
# * *****
###

def vogal (v):
    if v.lower() in "aeiou": # aqui testa com o operador in
        return True
    else:
        return False

def test_vogal_a():
    assert vogal("A") == True

def test_vogal_b():
    assert vogal("b") == False

def test_vogal_c():
    assert vogal("C") == False

def test_vogal_d():
    assert vogal("d") == False

def test_vogal_e():
    assert vogal("E") == True