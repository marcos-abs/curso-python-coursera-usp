#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: coeficiente_bionomial.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Tuesday, 16 January 2024 11:15:23
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Tuesday, 16 January 2024 11:32:41
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ conceito coefienciente binomial: https://www.somatematica.com.br/emedio/binomio/binomio1.php
# * *****
###

def fatorial(n):
  fat = 1
  while (n>1):
    fat = fat * n
    n = n - 1
  return fat

def testa_fatorial():
  if fatorial(0) == 1:
    print("Funciona para 0")
  else:
    print("Não funciona para 0")
  if fatorial(1) == 1:
    print("Funciona para 1")
  else:
    print("Não funciona para 1")
  if fatorial(2) == 2:
    print("Funciona para 2")
  else:
    print("Não funciona para 2")
  if fatorial(5) == 120:
    print("Funciona para 5")
  else:
    print("Não funciona para 5")

def numero_binomial(n, k):
  return fatorial(n) / (fatorial(k) * fatorial(n-k))

def testa_binomial():
  if numero_binomial(5, 2) == 10:
    print("Funciona para 5 e 2")
  else:
    print("Não funciona para 5 e 2")
  if numero_binomial(8, 3) == 56:
    print("Funciona para 8 e 3")
  else:
    print("Não funciona para 8 e 3")
  if numero_binomial(5, 2) == numero_binomial(5, 3): # 10 == 10
    print("Funciona propriedade 1")
  else:
    print("Não funciona propriedade 1")
  if numero_binomial(10, 6) == numero_binomial(10, 4): # 210 == 210
    print("Funciona propriedade 2")
  else:
    print("Não funciona propriedade 2")
  if numero_binomial(8, 1) == numero_binomial(8, 7): # 8 == 8
    print("Funciona propriedade 3")
  else:
    print("Não funciona propriedade 3")

testa_fatorial()
testa_binomial()

n = int(input("Digite o valor de n: "))
k = int(input("Digite o valor de k: "))
print("O resultado é ", numero_binomial(n, k))
