#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: test_fatorial.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Tuesday, 16 January 2024 12:50:22
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Tuesday, 16 January 2024 18:36:26
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············
# * *****
###

def fatorial (n):
  if n < 0:
    return 0
  i = fat = 1
  while i <= n:
    fat = fat * i
    i = i + 1
  return fat

def test_fatorial0():
  assert fatorial(0) == 1

def test_fatorial1():
  assert fatorial(1) == 1

def test_fatorial_negativo():
  assert fatorial(-10) == 0

def test_fatorial4():
  assert fatorial(4) == 24

def test_fatorial5():
  assert fatorial(5) == 120
