#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: test_maximo.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Tuesday, 16 January 2024 18:52:26
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Tuesday, 16 January 2024 19:35:13
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Exercício 1 - Máximo
# * Escreva a função maximo que recebe 2 números inteiros como parâmetro e devolve o maior deles.
# * *****
###

def maximo(x, y):
  if x > y:
    return x
  else:
    return y

def test_maximo0e1():
  assert maximo(0, 1) == 1

def test_maximo1e0():
  assert maximo(1, 0) == 1

def test_maximo0e0():
  assert maximo(0, 0) == 0

def test_maximo1e1():
  assert maximo(1, 1) == 1
