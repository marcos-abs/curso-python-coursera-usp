#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: test_maior_primo.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Tuesday, 16 January 2024 19:58:01
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Tuesday, 16 January 2024 20:35:59
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Exercício 2 - Primos
# * Escreva a função maior_primo que recebe um número inteiro maior ou igual a 2 como parâmetro
# * e devolve o maior número primo menor ou igual ao número passado à função
# * *****
###

def ehPrimo(k):
  if k == 1: # 1 não é primo!!! fonte: https://pt.wikipedia.org/wiki/N%C3%BAmero_primo
    return False
  i = 2
  while i < k:
    if k % i == 0:
      return False
    i += 1
  return True

def maior_primo(k):
  if k < 2:
    return 0
  i = k
  while i >= 2:
    if ehPrimo(i):
      return i
    i -= 1
  return 0

def test_maior_primo100():
  assert maior_primo(100) == 97

def test_maior_primo36():
  assert maior_primo(36) == 31

def test_maior_primo7():
  assert maior_primo(7) == 7

def test_ehPrimo100():
  assert ehPrimo(100) == False

def test_ehPrimo7():
  assert ehPrimo(7) == True

def test_ehPrimo3():
  assert ehPrimo(3) == True

def test_ehPrimo2():
  assert ehPrimo(2) == True

def test_ehPrimo1(): # 1 não é primo!!! fonte: https://pt.wikipedia.org/wiki/N%C3%BAmero_primo
  assert ehPrimo(1) == False

# resposta = True
# while(resposta):
#   n = int(input('Digite um número inteiro maior ou igual a 2: '))
#   print('O maior número primo menor ou igual a {} é {}.'.format(n, maior_primo(n)))
#   if n == 0:
#     resposta = False