#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: test_sample.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Tuesday, 16 January 2024 12:35:27
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Tuesday, 16 January 2024 12:36:22
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Instalação: pip3 install -U pytest
# * ············ Site: https://docs.pytest.org/en/latest/
# * *****
###

def func(x):
    return x + 1

def test_answer():
    assert func(3) == 4