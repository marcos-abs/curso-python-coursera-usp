#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: test_bhaskara_refatorado.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Tuesday, 16 January 2024 20:49:16
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Tuesday, 16 January 2024 21:12:41
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ bhaskara refatorado
# * *****
###

import math

def delta(a, b, c):
  return b ** 2 - 4 * a * c

def raizp(a, b, d): # raiz positiva # d == delta
  return (-b + math.sqrt(d)) / (2 * a)

def raizn(a, b, d): # raiz negativa # d == delta
  return (-b - math.sqrt(d)) / (2 * a)

def main():
  a = float(input("Digite o valor de a:"))
  b = float(input("Digite o valor de b:"))
  c = float(input("Digite o valor de c:"))
  imprime_raizes(a, b, c)

def imprime_raizes(a, b, c):
  d = delta(a, b, c)
  if d == 0:
    print("A única raiz possível é: ", raizp(a, b, d))
  else:
    if d < 0:
      print("Esta equação não possui raízes reais")
    else:
      print("A raiz positiva é: ", raizp(a, b, d), " e a raiz negativa é: ", raizn(a, b, d))

def test_delta20_5_20():
  assert delta(20, 5, 20) == -1575

def test_delta5_20_5():
  assert delta(5, 20, 5) == 300

def test_delta10_20_30():
  assert delta(10, 20, 30) == -800

def test_raizp5_20_300():
  assert raizp(5, 20, 300) == -0.26794919243112253

def test_raizn5_20_300():
  assert raizn(5, 20, 300) == -3.7320508075688776

# main()