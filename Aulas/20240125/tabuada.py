#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: tabuada.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Thursday, 25 January 2024 09:40:23
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Thursday, 25 January 2024 09:40:52
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············ Laços aninhados
# * *****
###

linha = 1
coluna = 1
while linha <= 10:
  while coluna <= 10:
    print(linha * coluna, end = "\t")
    coluna = coluna + 1
  linha = linha + 1
  print()
  coluna = 1