#!python3.7
# -*- coding: utf-8 -*-
###
# * *****
# * File: FatoresPrimos.py
# * Project: Curso de Python Coursera USP
# * Path: ~desenvolvedor/curso-de-python-coursera-usp
# * File Created: Thursday, 25 January 2024 10:08:38
# * Author: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Last Modified: Thursday, 25 January 2024 10:09:14
# * Modified By: Marcos Antônio Barbosa de Souza (desouza.marcos@uol.com.br)
# * -----
# * Copyright (c) 2024 All rights reserved, Marcant Tecnologia em Sistemas de Informação
# * -----
# * Description:
# * ············
# * *****
###

n = int(input("Digite um número inteiro > 1: "))
fator = 2
multiplicidade = 0
while n > 1:
  while n % fator == 0:
    multiplicidade = multiplicidade + 1
    n = n / fator
  if multiplicidade > 0:
    print("Fator", fator, "multiplicidade =", multiplicidade)
  fator = fator + 1
  multiplicidade = 0